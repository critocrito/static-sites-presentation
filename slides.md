# Cheap and secure websites.

The main takeaways:

- Choose tools wisely.
- Invest into tools that can be re-purposed.
- Think in process.
- A *radical* proposition to take tech back.

---

## Hello

My name is Christo, and I work for Tactical Tech, an organization based in
Berlin, Germany.

- christo@tacticaltech.org
- https://gitlab.com/critocrito/static-sites-presentation

---

## Process and Tools [1/4]

How can we collaborate and work, where ...

- ... we don't get frustrated.
- ... risk a burnout.
- ... produce results.
- ... accept that the world around us changes.

---

## Process and Tools [2/4]

We have to look at our process. A process is a set of activities that interact
to achieve a result.

- *Personal* process.
- *Group* process.

---

## Process and Tools [3/4]

We want to work:

- Distributed.
- Asynchronous.
- Collaborative.
- Getting Things Done.
- Excercise Ownership.

---

## Process and Tools [4/4]

Tools should ...

- ... enable us.
- ... not decide *how* we do things.
- ... retain our capacity of exerting power.
- ... not impose a culture into our communities.

---

## Website Creation [1/3]

Can we apply this thinking when we create websites?

---

## Website Creation [2/3]

Common approaches to websites architecture (php, drupal, wordpress, rails,
...):

- Webserver
- Appserver
- Database

---

## Website Creation [3/3]

Proposed approach to website architecture:

**Static Site Generation**.

- Webserver
- ~~Appserver~~
- ~~Database~~

- Increased security.
- Operational cheaper.

---

## Markdown [1/3]

- A *lightweight* markup language with plain text formatting syntax.
- Annotate raw text to give it a *structure*.
- Can easily be parsed my computers and *transformed*, e.g. HTML.

---

## Markdown [2/3]

- Start headers with `#`.
- Surround words with `*` to turn them italic or bold.
- `[This is a link](https://myshadow.org)`.
- Create lists using `-`.

...

---

## Markdown [3/3]

```
# A h1 header

Paragraphs are separated by blank lines. *Italic*, **bold**, and
`monospace`. Itemized lists look like:

- this one
- that one

## A h2 header

Embed code blocks:

    qsort []     = []
    qsort (p:xs) = (qsort lesser) ++ [p] ++ (qsort greater)
        where lesser  = filter (< p) xs
              greater = filter (>= p) xs
```

---

## Git [1/2]

- Keep track of changes in any files.
- Synchronize changes with peers.
- Diverge from the main line of development and continue to do work without
  messing with that main line.
- Collaborate distributed and asynchronous.

---

## Git [2/2]

- Takes some effort to learn.
- Data assurance.
- Add time to your project.
- Applicable in many different contexts.
- Process oriented.
- Doesn't enforce a workflow.
- Doesn't impose a workflow.

---

## Static Site Generation

```
Metalsmith(__dirname)
  .metadata({
    sitename: "My Static Site & Blog",
    siteurl: "http://example.com/",
  })
  .source('./src')            // source directory
  .destination('./build')     // destination directory
  .clean(true)                // clean destination before
  .use(collections({posts: 'posts/*.md'}))  // group all blog posts
  .use(markdown())            // transpile all md into html
  .use(permalinks({relative: false}) // change URLs to permalinks
  .use(layouts({engine: 'handlebars'}) // use any layout engine
  .build(function(err) {      // build process
    if (err) throw err;       // error handling is required
  });
```

---

## Examples for Static Sites [1/2]

https://myshadow.org/train

![Training Curriculum](./img/example1.png)

---

## Examples for Static Sites [2/2]

https://securityinabox.org

![Security In A Box](./img/example2.png)

---

## References

- markdown

  - https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#images

- git

  - https://git-scm.com/
  - https://git-scm.com/book/en/v2
